from tkinter import Label
from knob import Knob
import sys
sys.path.append('../')


"""
Documentation de la classe Presentation pour une piste

	Version : 1/04/2020 Thibaud BARON Equipe ROCCO

	Participant : Thibaud BARON

	Description :

		Présentation est une classe concrète qui va permettre d'afficher les boutons, les informations ,... à l'utilisateur pour une seule piste associée 

	Attribut :

		topPresentation : topPresentation où sera affichée la présentation de la piste
		idPiste : Numéro de la piste
		controller : Controller de la piste

		suivant : Presentation de la piste suivant  
		precedent : Presentation de la piste precedent
		
		idLabel : Texte affichant le numéro de la piste
		nameLabel : Texte affichant le nom du son
		loadButton : Bouton pour charger un son
		stereoLabel : Texte affichant "Stéréo"
		stereoKnob : bouton tournant pour sélectionner la valeur de la stéréo
		volumeLabel : Texte affichant "Volume"
		soundSlider : curseur pour sélectionner la valeur du volume
		speedLabel : Texte affichant "Pitch"
		speedKnob : Knob pour sélectionner la valeur de vitesse
		muteLabel : Texte pour afficher le "Mute"
		muteButtonState : Boolean pour si le son est mise en sourdine
		muteButton : Bouton pour mettre en sourdine
		effectButton : Bouton pour les effets
		crossFadeButton : Bouton pour activer le crossFade
		randomButton : Button pour activer le random

	Methode :
		open_file(self) : Méthode qui signale l'importation d'un son.
		modifyStereoValue(self) :
		modifySpeedValue(self) :
		modifyVolumeValue(self) :
		

"""


class Presentation (tkinter.Frame):

	"""
		Constructeur (Non fini)
		
		Param:
			Presentation		self : Objet créé
			Controller			controller : Controller de la piste
			top.Presentation	topPresentation : Presentation du top où sera affichée Presentation de la piste.
			int					idPiste : Numéro attribué à la piste pour se repérer
	"""	
	
	def __init__( self ,controller, topPresentation , idPiste):
		super().__init__(topPresentation)
		
		self.topPresentation = topPresentation
		self.controller = controller

		self.idPiste = idPiste
		self.suivant = None
		self.precedent = None

		self.idLabel = Label( self , text = "CH " + str( self.idPiste ) )
		self.idLabel.pack()

		self.nameLabel = Label( self , text="Veuillez charger un son")
		self.nameLabel.pack()

		self.LoadButton = Button( self , text="Charger" , command = self.open_file )
		self.LoadButton.pack()

		self.removeButton = Button( self , text="Supprimer" , command = self.askRemovePiste )

		self.stereoLabel = Label(self,text="Stéréo")
		self.stereoLabel.pack()

		self.stereoKnob = Knob(self, -1 , 1 , self.modifyStereoValue)
		self.stereoKnob.pack()

		self.volumeSlider = Scale(self , orient=Vertical , text="Volume")
		self.volumeSlider.pack()

		self.speedLabel = Label(self, text="Pitch")
		self.speedLabel.pack()

		self.speedKnob = Knob( self , -100 , 100 , self.modifySpeedValue)
		self.speedKnob.pack()

		self.muteLabel = Label( self , text = "Mute")
		self.muteLabel.pack()

		self.muteButtonState = False
		self.muteButton = Button (self, text="Mute")
		sound_off=tkinter.PhotoImage(file="../images/sound_off.png",master=self.master)
		self.muteButton.pack()

		self.effectButton = Button(self,text="Effet", command = self.modifyEffect)
		self.effectButton.pack()

		self.crossFadeButton = Button(self,text="CrossFade", command = self.modifyCrossFade)
		self.crossFadeButton.pack()

		self.randomButton = Button(self,text="Random", command = self.modifyRandom)
		self.randomButton.pack()

	"""
		Méthode qui signale l'importation d'un son.
	"""
	def open_file(self):
		self.controller.listenerLoadTrack ( self.idPiste )

	def setName(self, name):
		self.nameLabel.config(text = name)
		##self.nameLabel.pack() merci de vérifier si ça marche sans

	"""
		Méthode qui signale au contoller de modifier (si possible) la valeur du stéreo de la piste.
	"""
	def modifyStereoValue(self):
		self.controller.listenerStereo( self.stereoKnob.getValue() , self.idPiste )


	"""
		Méthode qui signale au contoller de modifier (si possible) la valeur de la vitesse de la piste.
	"""
	def modifySpeedValue(self):
		self.controller.listenerSpeed( self.speedButton.getValue() , self.idPiste )

	"""
		Méthode qui signale au contoller de modifier (si possible) la valeur du volume de la piste.
	"""
	def modifyVolumeValue(self):
		self.controller.listenerVolume( self.volumeButton.get() , self.idPiste )


	"""
		Méthode qui signale un demande de modifier un effet.
	"""
	def modifyEffect(self):
		##Code à insérer


	"""
		Méthode qui signale au contoller de faire un crossfade.
	"""
	def modifyCrossFade(self):
		##Code à insérer

	"""
		Méthode qui signale au contoller d'activer le Random.
	"""
	def modifyRandom(self):
		##Code à insérer



	def askRemovePiste(self):
		self.controller.removeChannelID( self.idPiste )

	def remove(self):
		self.destroy()

	def setId(self,id):
		self.idPiste = id
		self.idLabel.configure(text ="CH "+ str(self.idPiste))
		##self.idLabel.pack() merci de vérifier si ça marche sans

	def getId(self,id):
		return self.idPiste

	"""
		Méthode qui renvoie la Presentation de la piste suivante
		return : Presentation
	"""

	def getSuivant(self):
		return self.suivant


	"""
		Méthode qui attribut la Presentation de la piste suivante
		param :
				suivant		Presentation de la piste suivante
	"""
	def setSuivant(self,suivant):
		self.suivant=suivant

	"""
		Méthode qui renvoie la Presentation de la piste précédente
		return : Presentation
	"""
	def getPrecedent(self):
		return self.precedent



	"""
		Méthode qui attribut la Presentation de la piste précédente
		param :
				precedent	Presentation de la piste précédente
	"""
	def setPrecedent(self,precedent):
		self.Precedent=precedent

	"""
		Méthode qui retourne le boolean de si ça a été mise en sourdine
	"""
	def getMuted(self):
		return self.soundButtonState