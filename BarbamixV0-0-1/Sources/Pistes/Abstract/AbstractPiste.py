# -*- coding: utf-8 -*-

"""
Cree le mardi 31/03/2020

@author: L'Equipe Rocco // Corentin BRIAND
"""


class AbstractPiste ():
    """
    Classe abstraite de piste


    attributs
    ----------
    Volume : float
        Volume de la piste, doit être comprise en 0 et 100
    Stereo : float
        Valeur de la stereo de la piste, doit être comprise en -100 et 100
    Pitch : float
        Valeur du Pitch de la piste, doit être comprise en 0 et 200
    Mute : booleen
        Si la piste est mute (true) ou non (false)
    Precedent : AbstractPiste
        Channel précédent
    Suivant : AbstractPiste
        Channel suivant
    IdChannel : int
        ID de la piste
    Track : Track
        track de la piste pour sauvegarder le fichier en cours
    Effet : ???
        Les effets de la piste
    CrossFade : ???
        Le CrossFade de la piste
    RandomState : booleen
        Si le random est actif (true) ou non (false)
    RandomPlay : int
        Nombre de fois que le son devra être joué en ??? secondes


    Methodes
    -------
    getID() : int
        Retourne l'ID de la piste
        
    setID(int ID)
        Change l'ID de la piste
        
    getSuivant() : AbstractPiste
        Retourne la piste suivante
    
    setSuivant(AbstractPiste Suivant)
        Change la piste suivant
        
    getPrecedent() : AbstractPiste
        Retourne la piste precedente
        
    setPrecedent(AbstractPiste Precedent)
        Change le channel precédént de la piste
            
    getVolume() : float
        Retourne le volume de la piste
        
    setVolume(float Volume)
        Change le volume de la piste
        
    getStereo() : float
        Retourne la stéréo de la piste
    
    setStereo(float Stereo)
        Change la stéréo de la piste
        
    getPitch() : float
        Retourne le pitch de la piste
    
    setPitch(float Pitch)
        Change le pitch de la piste
        
    getMute() : boolean
        Retourne l'état du mute de la piste
    
    setMute(boolean Mute)
        Change l'état du mute de la piste
        
    getTrack() : Track
        Retourne le track de la piste
    
    setTrack(Track Track)
        Change le track de la piste
        
    getEffet(self) : ???
        Retourne les effets de la piste
    
    setEffet(??? Effet)
        Change les effets de la piste
        
    getCrossFade(self) : ???
        Retourne le CrossFade de la piste
    
    setCrossFade(??? CrossFade)
        Change le CrossFade de la piste
        
    getRandomState() : boolean
        Retourne l'état du random de la piste
    
    setRandomState(boolean RandomState)
        Change l'état du random de la piste
        
    getRandomPlay() : int
        Retourne le nombre de fois que le son doit être joué en ??? secondes
    
    setRandomPlay(int RandomPlay)
        Change le nombre de fois que le son doit être joué en ??? secondes
    """
    
    def __init__(self,ID):
        """
        Constructeur de piste
        
        
        Paramètres
        ----------
        ID : int
            L'ID de la piste
        """
        self.Volume=100
        self.Stereo=0
        self.Pitch=100
        self.Mute = False
        self.Precedent = None
        self.Suivant = None
        self.IdChannel = ID
        self.Track = None
        self.Effet = None
        self.CrossFade = None
        self.RandomState = False
        self.RandomPlay = 1

    def getID(self):
        """
        Retourne l'ID de la piste
        
        
        Return
        ----------
        int : L'ID de la piste
        """
        return self.IdChannel
    
    def setID(self,ID):
        """
        Change l'ID de la piste
        
        
        Parametres
        ----------
        ID : int
            Le nouvel ID de la piste
        """
        self.IdChannel=ID
    
    def getSuivant(self):
        """
        Retourne la piste suivante
        
        
        Return
        ----------
        AbstractPiste : La piste suivante
        """
        return self.Suivant
    
    def setSuivant(self,Suivant):
        """
        Change la piste suivant
        
        
        Parametres
        ----------
        Suivant : AbstractPiste
            La nouvelle piste suivante
        """
        self.Suivant=Suivant
            
    def getPrecedent(self):
        """
        Retourne la piste précédente
        
        
        Return
        ----------
        AbstractPiste : La piste précédente
        """
        return self.Precedent
    
    def setPrecedent(self,Precedent):
        """
        Change le channel précédent de la piste
        
        
        Parametres
        ----------
        Precedent : AbstractPiste
            La nouvelle piste précédent
        """
        self.Precedent=Precedent
            
    def getVolume(self):
        """
        Retourne le volume de la piste
        
        
        Return
        ----------
        float : Le volume de la piste
        """
        return self.volume
    
    def setVolume(self,Volume):
        """
        Change le volume de la piste
        
        
        Parametres
        ----------
        Volume : float
            Le nouveau volume de la piste
        """
        self.volume=Volume
        
    def getStereo(self):
        """
        Retourne la stéréo de la piste
        
        
        Return
        ----------
        float : la stéréo de la piste
        """
        return self.Stereo
    
    def setStereo(self,Stereo):
        """
        Change la stéréo de la piste
        
        
        Parametres
        ----------
        Stereo : float
            La nouvelle Stereo de la piste
        """
        self.Stereo=Stereo
        
    def getPitch(self):
        """
        Retourne le pitch de la piste
        
        
        Return
        ----------
        float : Le pitch de la piste
        """
        return self.Pitch
    
    def setPitch(self,Pitch):
        """
        Change le pitch de la piste
        
        
        Parametres
        ----------
        Pitch : float
            Le nouveau pitch de la piste
        """
        self.Pitch=Pitch
        
    def getMute(self):
        """
        Retourne l'état du mute de la piste
        
        
        Return
        ----------
        boolean : l'état de la piste
        """
        return self.Mute
    
    def setMute(self,Mute):
        """
        Change l'état du mute de la piste
        
        
        Parametres
        ----------
        Mute : boolean
            Le nouvel état du mute de la piste
        """
        self.Mute=Mute
        
    def getTrack(self):
        """
        Retourne Le track de la piste
        
        
        Return
        ----------
        Track : Le track de la piste
        """
        return self.Track
    
    def setTrack(self,Track):
        """
        Change le track de la piste
        
        
        Parametres
        ----------
        Track : Track
            Le nouveau track de la piste
        """
        self.Track=Track
        
    def getEffet(self):
        """
        Retourne les effets de la piste
        
        
        Return
        ----------
        ??? : Les effets de la piste
        """
        return self.Effet
    
    def setEffet(self,Effet):
        """
        Change les effets de la piste
        
        
        Parametres
        ----------
        Effet : ???
            Les nouveaux effets de la piste
        """
        self.Effet=Effet
        
    def getCrossFade(self):
        """
        Retourne le CrossFade de la piste
        
        
        Return
        ----------
        ??? : Le CrossFade de la piste
        """
        return self.CrossFade
    
    def setCrossFade(self,CrossFade):
        """
        Change le CrossFade de la piste
        
        
        Parametres
        ----------
        CrossFade : ???
            Le nouveau CrossFade de la piste
        """
        self.CrossFade=CrossFade
        
    def getRandomState(self):
        """
        Retourne l'état du random de la piste
        
        
        Return
        ----------
        boolean : L'état du random de la piste
        """
        return self.RandomState
    
    def setRandomState(self,RandomState):
        """
        Change l'état du random de la piste
        
        
        Parametres
        ----------
        RandomState : boolean
            Le nouvel état du random de la piste
        """
        self.RandomState=RandomState
        
    def getRandomPlay(self):
        """
        Retourne le nombre de fois que le son doit être joué en ??? secondes
        
        
        Return
        ----------
        int : Le nombre de fois que le son doit être joué en ??? secondes
        """
        return self.RandomPlay
    
    def setRandomPlay(self,RandomPlay):
        """
        Change le nombre de fois que le son doit être joué en ??? secondes
        
        
        Parametres
        ----------
        RandomPlay : int
            Le nouveau nombre de fois que le son doit être joué en ??? secondes
        """
        self.RandomPlay=RandomPlay