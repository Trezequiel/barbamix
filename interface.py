# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 08:50:24 2020

@author: Equipe Rocco

"""
# @Author: Nathan Mingoube, Groupe trébuchet
# @Name: Track class
# @Date: 02/02/2020
# @Version: 1.0.0
# @Dependencies: pyo librairies




# importe le module graphique TK
# Note : a refaire (import *)
from tkinter import *
import os
import tkinter as tk
import tkinter.filedialog

#Import de Pyo
from pyo import *


s = Server().boot()  #Starts audio engine
s.start()
        
class Track:
	"""
    path=""                     #Absolute path to sound file
    pitch=1.0                   #Pitch (highness) of the sound
    xfade=25                    #Crossfade (in % of total duration)
    mul=1                       #Multiplicator of sound speed
    start =0                    #Offset to start
    level=0                     #Sound level modifier
    gain=0                      #Gain modifier
    pan=0.0                     #PAN of the sound [-1.0;1.0]. Set to -1.0 to have the sound 100% to the right or 1.0 to have the sound 100% to the left (may be the other way around)
	"""

	def __init__(self,pth):
		self.path=pth                    #Absolute path to sound file
		self.srcTable=SndTable(self.path)
		self.looper=Looper(self.srcTable)
		self.resLooper=Looper(self.srcTable)
		self.pitch=1.0                   #Pitch (highness) of the sound
		self.xfade=25                    #Crossfade (in % of total duration)
		self.mul=1                       #Multiplicator of sound speed
		self.start =0                    #Offset to start
		self.level=0                     #Sound level modifier
		self.gain=0                      #Gain modifier
		self.pan=0.0                     #PAN of the sound [-1.0;1.0]. Set to -1.0 to have the sound 100% to the right or 1.0 to have the sound 100% to the left (may be the other way around)


	def setSource(self,file):        	#Sets source to the audio file
		path=file
		self.srcTable=SndTable(file)

	def soundDuration():       			#Returns the duration of the sound file
		self.srcTable.getDur()

	def setPitch(self,pitch):
		self.pitch=pitch

	def setLevel(self,lv):
		self.srcTable.mul=lv
		self.level=self.level+lv

	def setPan(self,pn):             	#Sets the pan
		self.pan=pn

	def initSound(self):           		#Initiates the looper class and summons its controls
		self.looper=Looper(table=self.srcTable,pitch=self.pitch,dur=180,start=0,xfade=self.xfade,autosmooth=True,mul=[self.level*(1+self.pan),self.level*(1-self.pan)])
	    
	def playSound(self):             	#Plays the sound
		self.resLooper=self.looper.mix(2).out()
	    
	    
class Channel ( tk.Frame ):
	def __init__( self , channelsFrame , master , idChannel,Track = None,soundFile=None):
		super().__init__(master)
		self.master = master			#Master de la piste (dependance à la fenetre)
		self.track = Track				#Track de la piste
		self.soundFile = soundFile		#Fichier son de la piste
		self.soundButtonState = False	#Etat du bouton mute

		#Creation du label du channel avec son ID
		self.Label = Label(self,text="CH "+idChannel,background="#ABCDEF")
		self.Label.pack()

		#Creation du nom du track du channel
		self.NameLabel = Label(self,text="Le son",background="#ABCDEF")
		self.NameLabel.pack();

		#Creation du boutton permettant de charger un son
		self.loadButton = Button(self, text= "Charger",background="#ABCDEF", command = self.open_file)
		self.loadButton.pack()

		#Creation du label stereo
		self.stereoLabel = Label(self,text="Stereo",background="#ABCDEF")
		self.stereoLabel.pack();
		#Creation du slider stereo, probleme de compatibilite avec PYO
		#self.stereoSlider = Scale (self, from_= -90 , to=90 , orient = HORIZONTAL,background="#ABCDEF"); #Le -90 et 90 est totalement inventé, faut plus de recherche
		#self.stereoSlider.pack();


		#Creation du label volume
		self.soundLabel = Label(self,text="Volume",background="#ABCDEF")
		self.soundLabel.pack();
		#Creation du slider volume, probleme de compatibilite avec PYO
		#self.soundSlider = Scale(self, from_=100, to=0, orient=VERTICAL,background="#ABCDEF")
		#self.soundSlider.pack()

		#Creation du label vitesse
		self.speedLabel = Label(self,text="Pitch",background="#ABCDEF")
		self.speedLabel.pack();
		#Creation du slider vitesse, probleme de compatibilite avec PYO
		#self.speedSlider = Scale (self, from_= -100 , to = 100 , orient = HORIZONTAL,background="#ABCDEF"); #Le -100 et 100 est totalement inventé, faut plus de recherche
		#self.speedSlider.pack();

		#Creation de l'image mute et creation du boutton associer
		sound_off=tk.PhotoImage(file="sound_off.png",master=master)
		self.Mute = Label(self,text="Muet",background="#ABCDEF")
		self.Mute.pack();
		self.soundButton = Button(self,image=sound_off, command = self.clickedSoundButton,background="#ABCDEF" )
		self.soundButton.image = sound_off
		self.soundButton.pack()


		#Creation des bouttons d'effet, crossFade et Random
		self.effetButton = Button(self,text= "Effet",background="#ABCDEF")
		self.effetButton.pack()
		self.CrossFade = Button(self,text= "CrossFade",background="#ABCDEF")
		self.CrossFade.pack()
		self.Random = Button(self,text= "Aléatoire",background="#ABC")
		self.Random.pack()

		#Configuration du channel
		self.configure(background="#FFFFFF")
		self.pack(side = LEFT)

		
	def getTrack(self):				#Accesseur du track de la piste
	    return self.track


	def setTrack(self,track):		#Mutateur du track de la piste
	    self.track=track
	    #play
		
		
	def clickedSoundButton(self):	#Methode permettant le switch entre l'image de son "mute" et celle de son "un-mute"
		if self.soundButtonState:
			sound_off=tk.PhotoImage(file="sound_off.png",master=self.master)
			self.soundButton.config(image=sound_off)
			self.soundButton.image = sound_off
			self.soundButtonState = False
		else:
			sound_on=tk.PhotoImage(file="sound_on.png",master=self.master)
			self.soundButton.config(image=sound_on)
			self.soundButton.image = sound_on
			self.soundButtonState = True

			
	def open_file(self):			#Methode permettant d'ouvrir un fichier de type wav ou mp3 et de le charger en tant que "track"
		self.soundFile = tk.filedialog.askopenfile(filetypes=[('wav file','.wav'),('mp3 file','.mp3')],title='Choose a file', initialdir=os.getcwd()+"/lib/")
		self.NameLabel.config(text=self.soundFile.name)
		self.NameLabel.pack()
		self.track = Track(self.soundFile.name)

		self.track.playSound()

class paramGlobal ( tk.Frame ):

	def __init__( self , channelsFrame , master):
		super().__init__(master)
		self.master = master				#Master (dependance à la fenetre)
		self.soundButtonState = False		#Etat du bouton mute

		#Creation du slider volume, probleme de compatibilite avec PYO
		#self.soundSlider = Scale(self, from_=100, to=0, orient=VERTICAL)
		#self.soundSlider.pack()

		#Creation de l'image mute et creation du boutton associer
		sound_off=tk.PhotoImage(file="pause.png",master=master)
		self.soundButton = Button(self,image=sound_off, command = self.clickedSoundButton )
		self.soundButton.image = sound_off
		self.soundButton.pack()

		#Creation du boutton de sauvegarde
		self.saveButton = Button(self,text= "Sauvegarder les paramètres")
		self.saveButton.pack()

		#Configuration du channel
		self.configure(background="#DBDAD5")
		self.pack(side = LEFT)
    
	
	def clickedSoundButton(self):			#Methode permettant le switch entre l'image de son "mute" et celle de son "un-mute"
		if self.soundButtonState:
			sound_off=tk.PhotoImage(file="pause.png",master=self.master)
			self.soundButton.config(image=sound_off)
			self.soundButton.image = sound_off
			self.soundButtonState = False
		else:
			sound_on=tk.PhotoImage(file="play.png",master=self.master)
			self.soundButton.config(image=sound_on)
			self.soundButton.image = sound_on
			self.soundButtonState = True
    
class Mixer(tk.Frame):
    
	def __init__(self,master):
		super().__init__(master) 			#Création de la fenetre
		self.master = master 				#dependance à la fenetre
		self.init_mixer() 					#on lance la fenetre

		
	def init_mixer(self): 					#création de la fenetre
		self.master.title("Mixer")

		#Creation du menu
		monMenu = Menu(self.master)
		self.master.config(menu=monMenu)
		fichier = Menu(monMenu, tearoff=0)
		fichier.add_command(label="Charger")
		fichier.add_command(label="Sauvegarder")
		fichier.add_command(label="Exporter")
		fichier.add_separator()
		fichier.add_command(label="Quitter", command=self.master.destroy)
		monMenu.add_cascade(label="Fichier",menu=fichier)

		#Master des channels
		channels = Frame(self.master)

		#Création des pistes
		channel1 = Channel(channels, self.master , "1")
		channel2 = Channel(channels, self.master , "2")
		channel3 = Channel(channels, self.master , "3")
		channel4 = Channel(channels, self.master , "4")
		channel5 = Channel(channels, self.master , "5")
		channel6 = Channel(channels, self.master , "6")
		channel7 = Channel(channels, self.master , "7")
		channel8 = Channel(channels, self.master , "8")

		#Piste global
		globVol = paramGlobal(channels, self.master)



# Création d'une fenêtre
fen = Tk()
fen.resizable(width=False, height=False)
fen.geometry("1200x475")
fen.configure(background='#000')

# Création de l'application & lancement
app = Mixer(fen)
fen.mainloop()
